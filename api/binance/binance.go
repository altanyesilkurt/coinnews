package coinbase

import (
	"encoding/json"
	"fmt"

	"github.com/gorilla/websocket"

	"github.com/gin-gonic/gin"
)

const Address = "wss://stream.binance.com/ws/btcusdt@aggTrade"

const (
	TypeSubscribe   string = "subscribe"
	TypeUnSubscribe string = "unsubscribe"
)

type Message struct {
	Method string   `json:"method"`
	Params []string `json:"params"`
	Id     int      `json:"id"`
}

type Trade struct {
	EventType string `json:"e"`
	Symbol    string `json:"s"`
	EventTime int64  `json:"E"`
	Price     string `json:"p"`
}

func InitWebsocketClient(c *gin.Context) {

	conn, _, err := websocket.DefaultDialer.Dial(Address, nil)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	// sub := Message{
	// 	Method: TypeSubscribe,
	// 	Params: []string{"btcusdt@aggTrade"},
	// 	Id:     1,
	// }

	//conn.WriteJSON(sub)

	for {
		_, message, err := conn.ReadMessage() // 3️⃣
		if err != nil {
			panic(err)
		}
		// unmarshal the message
		var trade Trade
		json.Unmarshal(message, &trade) // 4️⃣

		fmt.Println(trade)

	}
}

//auto connect
//routing chann
