package binanceSocket

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"github.com/gorilla/websocket"

	"github.com/gin-gonic/gin"
)

const Address = "wss://stream.binance.com/ws/"

const (
	TypeSubscribe   string = "subscribe"
	TypeUnSubscribe string = "unsubscribe"
)

type Message struct {
	Method string   `json:"method"`
	Params []string `json:"params"`
	Id     int      `json:"id"`
}

type Trade struct {
	e string `json:"e"`
	s string `json:"s"`
	p string `json:"p"`
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func InitWebsocketClient(c *gin.Context) {
	var sb strings.Builder

	pairSymbol := c.Param("pairSymbol")
	sb.WriteString(Address)

	sb.WriteString(strings.ToLower(pairSymbol))
	sb.WriteString("@aggTrade")

	ws, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		log.Println(err)
	}

	conn, _, err := websocket.DefaultDialer.Dial(sb.String(), nil)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	for {
		mt, message, err := conn.ReadMessage() // 3️⃣
		if err != nil {
			panic(err)
		}
		var trade Trade
		json.Unmarshal(message, &trade) // 4️⃣

		ws.WriteMessage(mt, []byte(fmt.Sprintf("%+v", trade)))
	}
}

//auto connect
//routing chann
