package coinbase

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/gorilla/websocket"

	"github.com/gin-gonic/gin"
)

const Address = "wss://ws-feed.exchange.coinbase.com"

//const Address = "wss://stream.binance.com/ws/btcusdt@aggTrade"

const (
	ChannelTicker   string = "ticker"
	TypeSubscribe   string = "subscribe"
	TypeUnSubscribe string = "unsubscribe"
)

type Message struct {
	Type       string   `json:"type"`
	ProductIDs []string `json:"product_ids"`
	Channels   []string `json:"channels"`
}

type Trade struct {
	Type    string    `json:"type"`
	Time    time.Time `json:"time"`
	Product string    `json:"product_id"`
	Price   string    `json:"price"`
}

func InitWebsocketClient(c *gin.Context) {

	conn, _, err := websocket.DefaultDialer.Dial(Address, nil)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	sub := Message{
		Type:       TypeSubscribe,
		ProductIDs: []string{"BTC-USD"},
		Channels:   []string{ChannelTicker},
	}

	conn.WriteJSON(sub)

	for {
		_, message, err := conn.ReadMessage() // 3️⃣
		if err != nil {
			break
		}
		// unmarshal the message
		var trade Trade
		json.Unmarshal(message, &trade) // 4️⃣

		fmt.Println(trade)

	}
}
