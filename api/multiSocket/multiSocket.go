package multiSocket

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/websocket"

	"github.com/gin-gonic/gin"
)

const BinanceAddress = "ws://localhost:8080/binancemulti/"
const BtcTurkAddress = "ws://localhost:8080/btcturkmulti/"

const (
	TypeSubscribe   string = "subscribe"
	TypeUnSubscribe string = "unsubscribe"
)

type Message struct {
	Type    int    `json:"type"`
	Event   string `json:"event"`
	Join    bool   `json:"join"`
	Channel string `json:"channel"`
}

type Trade struct {
	Symbol                   string
	EventTime                string
	BinancePrice             string
	BinanceBtctukPercentage  float64
	BtcTurkPrice             string
	BtcTurkBinancepercentage float64
}

type BinanceTrade struct {
	Symbol string `json:"s"`
	Price  string `json:"p"`
}

type Response struct {
	int          `json:""`
	BtcturkTrade `json:""`
}

type BtcturkTrade struct {
	PairSymbol string `json:"PS"`
	Amount     string `json:"A"`
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func InitWebsocketClient(c *gin.Context) {

	ws, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		log.Println(err)
	}
	var binanceStringBuilder strings.Builder
	var btcturkStringBuilder strings.Builder

	pairSymbol := c.Param("pairSymbol")
	binanceStringBuilder.WriteString(BinanceAddress)
	btcturkStringBuilder.WriteString(BtcTurkAddress)

	binanceStringBuilder.WriteString(pairSymbol)
	btcturkStringBuilder.WriteString(pairSymbol)

	fmt.Println(btcturkStringBuilder.String())

	binanceConn, _, err := websocket.DefaultDialer.Dial(binanceStringBuilder.String(), nil)
	if err != nil {
		fmt.Println("Binance connection reconnected")
	}
	defer binanceConn.Close()

	fmt.Println("binance connection sağlandı")

	btcturkConn, _, err := websocket.DefaultDialer.Dial(btcturkStringBuilder.String(), nil)
	if err != nil {
		panic(err)
	}
	defer btcturkConn.Close()

	sub := Message{
		Type:    151,
		Event:   c.Param("pairSymbol"),
		Channel: "ticker",
		Join:    true,
	}
	x := [2]any{151, sub}

	btcturkConn.WriteJSON(x)
	fmt.Println("btcturk connection sağlandı")

	for {
		mt, binanceMessage, err := binanceConn.ReadMessage() // 3️⃣
		if err != nil {
			panic(err)
		}

		var binanceTrade BinanceTrade
		var btcturkSocketTrade BtcturkTrade
		var response []Response
		if err := json.Unmarshal(binanceMessage, &binanceTrade); err != nil {
			panic(err)
		}

		mt, btcturkMessage, err := btcturkConn.ReadMessage() // 3️⃣
		if err != nil {
			panic(err)
		}

		str1 := string(btcturkMessage[:])

		json.Unmarshal([]byte(str1), &response) // 4️⃣

		btcturkSocketTrade = response[1].BtcturkTrade

		t := time.Now()
		formattedTime := fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02d",
			t.Year(), t.Month(), t.Day(),
			t.Hour(), t.Minute(), t.Second())

		binanceFloat, _ := strconv.ParseFloat(binanceTrade.Price, 64)
		btcturkFloat, _ := strconv.ParseFloat(btcturkSocketTrade.Amount, 64)

		binanceBtctukPercentage, _ := strconv.ParseFloat(fmt.Sprintf("%.4f", calculatePrice(binanceFloat, btcturkFloat)), 64)
		btcturkBinancePercentage, _ := strconv.ParseFloat(fmt.Sprintf("%.4f", calculatePrice(btcturkFloat, binanceFloat)), 64)

		trade := Trade{
			Symbol:                   binanceTrade.Symbol,
			EventTime:                formattedTime,
			BinancePrice:             binanceTrade.Price,
			BinanceBtctukPercentage:  binanceBtctukPercentage,
			BtcTurkPrice:             btcturkSocketTrade.Amount,
			BtcTurkBinancepercentage: btcturkBinancePercentage,
		}

		ws.WriteMessage(mt, []byte(fmt.Sprintf("%+v", trade)))
	}
}

func calculatePrice(first float64, second float64) float64 {
	return 100 * ((first - second) / ((first + second) / 2))
}

//auto connect
//routing chann
