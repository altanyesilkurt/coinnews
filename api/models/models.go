package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ToDoList struct {
	ID     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Task   string             `json:"task,omitempty" binding:"required,min=3"`
	Status bool               `json:"status,omitempty"`
}

type SocketModel struct {
	RequestID int    `json:"requestID,omitempty" bson:"requestID,omitempty"`
	Command   string `json:"task,omitempty" binding:"required,min=3"`
}
