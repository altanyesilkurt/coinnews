package btcturkSocket

import (
	"log"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

const Address = "wss://ws-feed-pro.btcturk.com"

const (
	TypeSubscribe   string = "subcribe"
	TypeUnSubscribe string = "unsubscribe"
)

type Message struct {
	Type    int    `json:"type"`
	Event   string `json:"event"`
	Join    bool   `json:"join"`
	Channel string `json:"channel"`
}

type Response struct {
	int   `json:""`
	Trade `json:""`
}

type Trade struct {
	P string `json:"PS"`
	A string `json:"A"`
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func InitWebsocketClient(c *gin.Context) {

	conn, _, err := websocket.DefaultDialer.Dial(Address, nil)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	ws, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		log.Println(err)
	}

	sub := Message{
		Type:    151,
		Event:   c.Param("pairSymbol"),
		Channel: "ticker",
		Join:    true,
	}
	x := [2]any{151, sub}

	conn.WriteJSON(x)

	for {
		mt, message, err := conn.ReadMessage() // 3️⃣
		if err != nil {
			panic(err)
		}

		ws.WriteMessage(mt, message)
	}
}

//auto connect
//routing chann
