package router

import (
	binance "main/binance"
	btcturk "main/btcturk"
	coinbase "main/coinbase"
	controller "main/controller"

	binanceMultiSocket "main/binanceMultiSocket"
	binanceSocket "main/binanceSocket"
	btcturkMultiSocket "main/btcturkMultiSocket"
	btcturkSocket "main/btcturkSocket"
	multiSocket "main/multiSocket"

	"github.com/gin-gonic/gin"
)

// Router is exported and used in main.go
func Router() *gin.Engine {

	r := gin.Default()

	r.Use(gin.Logger())

	// Recovery middleware recovers from any panics and writes a 500 if there was one.
	r.Use(gin.Recovery())

	r.Use(func() gin.HandlerFunc {
		return func(c *gin.Context) {
			c.Writer.Header().Set("Cache-Control", "public, max-age=604800, immutable")
			c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
			c.Writer.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		}
	}())

	r.GET("/api/task", controller.GetAllTask)
	r.GET("/api/coinbase", coinbase.InitWebsocketClient)
	r.GET("/api/binance", binance.InitWebsocketClient)
	r.GET("/api/btcturk", btcturk.InitWebsocketClient)
	r.POST("/api/task", controller.CreateTask)
	r.PUT("/api/task/:id", controller.TaskComplete)
	r.PUT("/api/undoTask/:id", controller.UndoTask)
	r.DELETE("/api/deleteTask/:id", controller.DeleteTask)
	r.DELETE("/api/deleteAllTask", controller.DeleteAllTask)
	r.GET("/binance/:pairSymbol", binanceSocket.InitWebsocketClient)
	r.GET("/btcturk/:pairSymbol", btcturkSocket.InitWebsocketClient)
	r.GET("/binancemulti/:pairSymbol", binanceMultiSocket.InitWebsocketClient)
	r.GET("/btcturkmulti/:pairSymbol", btcturkMultiSocket.InitWebsocketClient)
	r.GET("/multisocket/:pairSymbol", multiSocket.InitWebsocketClient)
	return r
}
