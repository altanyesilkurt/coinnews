package btcturk

import (
	"encoding/json"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

const Address = "wss://ws-feed-pro.btcturk.com"

const (
	TypeSubscribe   string = "subcribe"
	TypeUnSubscribe string = "unsubscribe"
)

type Message struct {
	Type    int    `json:"type"`
	Event   string `json:"event"`
	Join    bool   `json:"join"`
	Channel string `json:"channel"`
}

type Response struct {
	int   `json:""`
	Trade `json:""`
}

type Trade struct {
	PairSymbol string `json:"PS"`
	Amount     string `json:"A"`
}

func InitWebsocketClient(c *gin.Context) {

	conn, _, err := websocket.DefaultDialer.Dial(Address, nil)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	sub := Message{
		Type:    151,
		Event:   "BTCTRY",
		Channel: "ticker",
		Join:    true,
	}
	x := [2]any{151, sub}

	conn.WriteJSON(x)

	for {
		_, message, err := conn.ReadMessage() // 3️⃣
		if err != nil {
			panic(err)
		}

		var trade Trade
		var response []Response

		str1 := string(message[:])

		json.Unmarshal([]byte(str1), &response) // 4️⃣

		trade = response[1].Trade

		fmt.Println(trade)
	}
}

//auto connect
//routing chann
