package controller

import (
	"net/http"

	"main/models"
	service "main/service"

	"github.com/gin-gonic/gin"
)

// GetAllTask get all the task route
func GetAllTask(c *gin.Context) {
	payload := service.GetAllTask()

	c.IndentedJSON(http.StatusOK, payload)
}

// CreateTask create task route
func CreateTask(c *gin.Context) {

	var task models.ToDoList

	if err := c.ShouldBindJSON(&task); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"ERROR": err.Error(), "Message": "Invalid inputs. Please check your inputs"})
		return
	}

	var result = service.InsertOneTask(task)

	c.IndentedJSON(http.StatusOK, gin.H{"data": service.GetSingleTask(result)})
}

// TaskComplete update task route
func TaskComplete(c *gin.Context) {
	userUid := c.Param("id")
	service.TaskComplete(userUid)
	c.IndentedJSON(http.StatusOK, gin.H{"data": service.GetSingleTask(userUid)})
}

// UndoTask undo the complete task route
func UndoTask(c *gin.Context) {
	userUid := c.Param("id")
	service.UndoTask(userUid)
	c.IndentedJSON(http.StatusOK, gin.H{"data": service.GetSingleTask(userUid)})
}

// DeleteTask delete one task route
func DeleteTask(c *gin.Context) {
	userUid := c.Param("id")
	service.DeleteOneTask(userUid)
	c.IndentedJSON(http.StatusOK, "")

	// json.NewEncoder(w).Encode("Task not found")
}

// DeleteAllTask delete all tasks route
func DeleteAllTask(c *gin.Context) {
	count := service.DeleteAllTask()
	c.IndentedJSON(http.StatusOK, count)

	// json.NewEncoder(w).Encode("Task not found")
}
